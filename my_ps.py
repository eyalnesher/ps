#!/usr/bin/python
import os								#the code use the module os

path = '/proc'
proc_dir = os.listdir(path)						#we set the variable proc_dir to be all the directories in the directory /proc

dir_int = []

for i in proc_dir:
     if i.isdigit():
             dir_int.append(i)						#filter all the directories in /proc which their name consist of onley digits

stats = []
for a_dir in dir_int:
	path = '/proc/' + a_dir						#get the full path of the directories
	pid_dir = os.listdir(path)					#for each dirertory from the directories we took earlier we took all the directories inside
	for x in pid_dir:
		if x == 'stat':
			infor = []					#we took all the directories in the directories in dir_int called "stat"
			single_stat = open(path + '/' + x, 'r')		#we're looking in the "stst" directory
			stat_info = single_stat.read()
			for val in stat_info.split():			#split the value of stat_read into words
				infor.append(val)			#we're taking all the values in stat_info to a one list
			print 'pid: ' + infor[0]			#we're taking the pid - the first parameter
			print 'ppid: ' + infor[3]			#we're taking the ppid - the fourth parameter
			print 'comm - ' + infor[1]			#we're taking the comm - the second parameter
			print 'utime - ' + infor[13]			#we're taking the utime - the fourteenth parameter
			print 'num_threads - ' + infor[19]		#we're taking the num_threads - the nineteenth parameter
			print 'processor - ' + infor[38]		#we're taking the processor - the thiryt-eighth parameter
			print 'vsize - ' + infor[22] + '\n'		#we're taking the vsize - the twenty-second parameter
		
			single_stat.close()				#the file we just open shuld be closed after the usage

